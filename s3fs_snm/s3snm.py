"""
Ce module définit l'interface cli pour monter des partages s3fs
"""
import logging
import os
import stat
import sys
import traceback
from glob import glob

import yaml
from plumbum import cli, colors, local
from tendo import singleton

from s3fs_snm import __version__


def load_yaml(filename):
    with open(filename) as file:

        val_s3_conf = yaml.load(file, Loader=yaml.FullLoader)
        s = os.stat(filename)

    if val_s3_conf:
        for i in val_s3_conf:
            if not set(["name", "id", "secret"]).issubset(set(val_s3_conf[i].keys())):
                raise Exception(f"Ce fichier n'est pas un fichier .val_s3fs.yml valide: {filename}")
            if bool(s.st_mode & stat.S_IROTH) or bool(s.st_mode & stat.S_IRGRP):
                raise Exception(f"Ce fichier n'a pas les bonnes permissions (600): {filename}")
    else:
        logging.error(f"Le fichier {filename} est vide.")
    return val_s3_conf


def read_conf(val_s3_filename, callback=None):
    file_stats = os.stat(val_s3_filename)
    uid = file_stats.st_uid
    gid = file_stats.st_gid
    folder_path = os.path.dirname(val_s3_filename)
    folder_name = os.path.basename(folder_path)
    s3fs_conf = load_yaml(val_s3_filename)

    if callback and s3fs_conf:
        for bucket in s3fs_conf:
            bucket_dir = f"{folder_path}/{bucket}"
            callback(filename=val_s3_filename,
                     s3fs_conf=s3fs_conf,
                     folder_path=folder_path,
                     folder_name=folder_name,
                     bucket_dir=bucket_dir,
                     bucket=bucket,
                     uid=uid,
                     gid=gid,
                     )
    return val_s3_filename, uid, gid, folder_path, folder_name, s3fs_conf


def list_s3fs(filename, bucket_dir, folder_name, uid, s3fs_conf, **args):
    if s3fs_conf:
        logging.info(f"[OK] {bucket_dir}. uid:{uid}")


def generate_export_filename(folder_name, bucket):
    export_filename = f"/etc/exports.d/s3snm-{folder_name}-{bucket}.exports"
    return export_filename


def check(bucket_dir, s3fs_conf, folder_name, bucket, **args):
    if s3fs_conf:

        if not os.path.exists(bucket_dir):
            logging.warning(f"{bucket_dir} n'existe pas.")
        elif not os.path.ismount(bucket_dir):
            logging.warning(f"{bucket_dir} n'est pas un point de montage.")
        else:
            logging.info(f"[OK] {bucket_dir}.")

        export_filename = generate_export_filename(folder_name, bucket)
        if not os.path.exists(export_filename):
            logging.error(f"L'export NFS {export_filename} n'existe pas.")


def mount(bucket_dir, s3fs_conf, bucket, uid, gid, **args):
    if s3fs_conf:

        if not os.path.exists(bucket_dir):
            os.makedirs(bucket_dir)

        if not os.path.ismount(bucket_dir):
            logging.info(f"Montage du dossier {bucket_dir}")
            with local.env(AWSACCESSKEYID=s3fs_conf[bucket]['id'],
                           AWSSECRETACCESSKEY=s3fs_conf[bucket]['secret']):
                s3fs = local["s3fs"]
                exit_code, stdo, stde = s3fs[s3fs_conf[bucket]['name'],
                                             bucket_dir,
                                             '-o', 'url=https://s3.valeria.science',
                                             '-o', f'uid={uid}',
                                             '-o', f'gid={gid}',
                ].run()
                if len(stde) != 0:
                    logging.error(f"ERREUR lors du montage du dossier {bucket_dir}", exit_code, stdo, stde)
                    return 1
    return 0


def fix_exports(bucket_dir, uid, folder_name, bucket, s3fs_conf, **args):
    if s3fs_conf:

        export = f"{bucket_dir} " \
                 f"ul-pca-pr-*(rw,root_squash,crossmnt,fsid={uid}) " \
                 f"login.valeria.science(rw,root_squash,crossmnt,fsid={uid}) " \
                 f"10.250.200.0/24(rw,root_squash,crossmnt,fsid={uid})"

        export_filename = generate_export_filename(folder_name, bucket)
        export_need_a_fix = False
        if not os.path.exists(export_filename):
            export_need_a_fix = True
        else:
            with open(export_filename) as f:
                if not export in f.read():
                    export_need_a_fix = True
        if export_need_a_fix:
            logging.info(f"Création de l'export {export_filename}")
            logging.info(f"Point de montage de l'export NFS: {bucket_dir} avec uid: {uid}")
            with open(export_filename, 'w') as f:
                f.write(export)
            exportfs = local["/sbin/exportfs"]
            exit_code, stdo, stde = exportfs["-ra"].run()
            if exit_code != 0:
                logging.error(f"Erreur lors de l'exportfs: export_need_a_fix:{export_need_a_fix} "
                              f"export_filename:{export_filename} bucket_dir:{bucket_dir} uid:{uid} bucket:{bucket}")
    return 0


def orphan_mount_point(val_s3_filename, uid, gid, folder_path, folder_name, s3fs_conf, **args):
    all_folders = glob(f"{folder_path}/**/")
    bucket_dirs = []

    if s3fs_conf:
        for bucket in s3fs_conf:
            bucket_dirs += [f"{folder_path}/{bucket}/"]
    else:
        bucket_dirs += ["error_empty_config_file"]

    non_bucket_folders = set(all_folders) - set(bucket_dirs)
    for nbf in non_bucket_folders:
        if os.path.ismount(nbf):
            logging.warning(f"Le dossier {nbf} est un point de montage mais n'est pas listé dans le fichier "
                            f"{val_s3_filename}")


def orphan_exports(val_s3_filename, uid, gid, folder_path, folder_name, s3fs_conf, **args):
    all_exports = glob(f"/etc/exports.d/s3snm-{folder_name}-*.exports")
    configured_export_filenames = []

    if s3fs_conf:
        for bucket in s3fs_conf:
            configured_export_filenames += [generate_export_filename(folder_name, bucket)]
    else:
        configured_export_filenames = ["error_empty_config_file"]

    non_export_filenames = set(all_exports) - set(configured_export_filenames)
    for nef in non_export_filenames:
        logging.warning(f"Le fichier {nef} est exporté mais n'est pas défini dans "
                        f"{val_s3_filename}")


class S3SNM(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Cet outil en ligne de commande permet maintenir " \
                  "des points de montage s3fs à partir de fichiers de configuration yaml."
    COLOR_MANDATORY = colors.bold & colors.yellow

    @staticmethod
    def pre_run():
        root = logging.getLogger()
        root.setLevel(logging.INFO)

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        root.addHandler(handler)

        try:
            me = singleton.SingleInstance()
            S3SNM.run()
        except Exception as e:
            logging.error(f"{traceback.format_exc()} \nUne exception a été levée!: {e}")

    def __init__(self, *args):
        super().__init__(*args)

    def main(self, *args):
        if args:
            logging.error("Commande inconnue {0!r}".format(args[0]))
            return 1
        if not self.nested_command:
            logging.error("Aucune commande choisie. Faire --help pour l'aide")
            return 1


@S3SNM.subcommand("list")
class S3SNMList(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Permet de lister les fichiers yaml existants"

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            read_conf(yaml_filename, list_s3fs)
        return 0


@S3SNM.subcommand("check")
class S3SNMCheck(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Permet de vérifier les points de montage"

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            read_conf(yaml_filename, check)
        return 0


@S3SNM.subcommand("mount")
class S3SNMMount(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Monte les dossiers S3 avec s3fs"

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            read_conf(yaml_filename, mount)
        return 0


@S3SNM.subcommand("exportfs")
class S3SNMExportFS(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Mettre à jour les fichiers d'export NFS"

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            read_conf(yaml_filename, fix_exports)
        return 0


@S3SNM.subcommand("mount_and_export")
class S3SNMMountAndExportFS(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Monter les partages et mettre à jour les fichiers d'export NFS"

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            read_conf(yaml_filename, mount)
            read_conf(yaml_filename, fix_exports)
            orphan_mount_point(*read_conf(yaml_filename))
            orphan_exports(*read_conf(yaml_filename))
        return 0


@S3SNM.subcommand("orphan")
class S3SNMOrphan(cli.Application):
    VERSION = __version__
    DESCRIPTION = "Chercher les configs perdues ou les points de montage à l'abandon."

    @cli.positional(cli.ExistingDirectory)
    def main(self, root_s3fs_dir):
        r = glob(f"{root_s3fs_dir}/*/.val_s3fs.yml")
        for yaml_filename in r:
            orphan_mount_point(*read_conf(yaml_filename))
            orphan_exports(*read_conf(yaml_filename))
        return 0


if __name__ == "__main__":
    S3SNM.pre_run()
